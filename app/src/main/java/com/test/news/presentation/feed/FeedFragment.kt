package com.test.news.presentation.feed

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.test.news.R
import com.test.news.data.remote.controller.Resource
import com.test.news.data.remote.responses.NewsResponse
import com.test.news.databinding.FragmentFeedBinding
import com.test.news.model.Feed
import com.test.news.utils.EventObserver
import com.test.news.utils.ViewModelFactory
import com.test.news.utils.listeners.RecyclerItemListener
import dagger.android.support.AndroidSupportInjection
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class FeedFragment : Fragment(), RecyclerItemListener<Feed> {

    lateinit var binding: FragmentFeedBinding
    private lateinit var currentPhotoPath: String

    private val permissions = arrayOf(
        WRITE_EXTERNAL_STORAGE,
        READ_EXTERNAL_STORAGE
    )


    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel: FeedViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(FeedViewModel::class.java)
    }

    var newsList = arrayListOf<Feed>()

    @Inject
    lateinit var newsAdapter: FeedAdapter



    private val requestMultiplePermissions =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            if (permissions[READ_EXTERNAL_STORAGE] == true && permissions[WRITE_EXTERNAL_STORAGE] == true) {
                binding.cameraIcon.performClick()
            }
        }

    private val resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                showFillFeedDataBottomSheetDialog()
            }
        }



    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFeedBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        setupRecyclerView()
        // loading news data from api
        // i put the default query tesla because the api doesn't allow empty query
        viewModel.geNews("tesla")
        setupObservers()
        setUpListeners()
        scheduleDeleteTask()
        setUpDragAndDropItemListener()
        return binding.root
    }

    private fun setUpListeners() {
        binding.addImageTextView.setOnClickListener {
            binding.cameraIcon.performClick()
        }

        binding.switchPhoto.setOnClickListener {
            switchBetweenDynamicAndFixedPicturesMode()
        }

        binding.searchEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                // load feed depending on query value
                binding.searchProgressBar.visibility = View.VISIBLE
                binding.searchIcon.visibility = View.GONE
                viewModel.geNews(p0.toString())
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })
        binding.cameraIcon.setOnClickListener {
            if (ContextCompat.checkSelfPermission(requireActivity(), WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(requireActivity(), READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED
            ) {
                // Permission granted!. take a picture
                dispatchTakePictureIntent()
            } else {
                // request a permission
                requestMultiplePermissions.launch(permissions)
            }
        }
    }

    private fun setUpDragAndDropItemListener() {
        //rearrange the positions of the item.
        val itemTouchHelper = ItemTouchHelper(object : ItemTouchHelper.Callback() {
            override fun isLongPressDragEnabled() = true
            override fun isItemViewSwipeEnabled() = false

            override fun getMovementFlags(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder
            ): Int {
                val dragFlags =
                    ItemTouchHelper.UP or ItemTouchHelper.DOWN or ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
                val swipeFlags =
                    if (isItemViewSwipeEnabled) ItemTouchHelper.START or ItemTouchHelper.END else 0
                return makeMovementFlags(dragFlags, swipeFlags)
            }

            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                if (viewHolder.itemViewType != target.itemViewType)
                    return false
                val fromPosition = viewHolder.adapterPosition
                val toPosition = target.adapterPosition
                val item = newsList.removeAt(fromPosition)
                newsList.add(toPosition, item)
                recyclerView.adapter!!.notifyItemMoved(fromPosition, toPosition)
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                newsList.removeAt(position)
                binding.newsRecyclerView.adapter!!.notifyItemRemoved(position)
            }
        })
        itemTouchHelper.attachToRecyclerView(binding.newsRecyclerView)
    }

    private fun switchBetweenDynamicAndFixedPicturesMode() {
        newsAdapter.dynamicView = !newsAdapter.dynamicView
        if (newsAdapter.dynamicView) {
            val staggeredLayoutManager =
                StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            staggeredLayoutManager.gapStrategy =
                StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS
            binding.newsRecyclerView.layoutManager = staggeredLayoutManager

        } else {
            binding.newsRecyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
        }
    }

    private fun setupRecyclerView() {
        newsAdapter.setListener(this)
        newsAdapter.submitList(arrayListOf())
        binding.newsRecyclerView.adapter = newsAdapter
    }

    private fun setupObservers() {
        // listen to api result
        viewModel.newsResponse.observe(
            requireActivity(),
            EventObserver
                (object : EventObserver.EventUnhandledContent<Resource<NewsResponse>> {
                override fun onEventUnhandledContent(t: Resource<NewsResponse>) {
                    when (t) {
                        is Resource.Loading -> {
                            // show progress bar and remove no data layout while loading
                            binding.noDataLayout.visibility = View.GONE
                            binding.newsProgressBar.visibility = View.VISIBLE
                        }
                        is Resource.Success -> {
                            // response is ok get the data and display it in the list
                            binding.newsProgressBar.visibility = View.GONE
                            binding.searchProgressBar.visibility = View.GONE
                            binding.searchIcon.visibility = View.VISIBLE

                            val response = t.response as NewsResponse
                            newsList.clear()
                            newsAdapter.notifyDataSetChanged()
                            response.articles.forEach {
                                newsList.add(
                                    Feed(
                                        it.title,
                                        it.description,
                                        it.urlToImage,
                                        false,
                                        -1L
                                    )
                                )
                            }
                            newsAdapter.submitList(newsList)
                            viewModel.saveFeed(newsList)
                            if (newsList.isEmpty())
                                binding.noDataLayout.visibility = View.VISIBLE
                            else
                                binding.noDataLayout.visibility = View.GONE

                        }
                        is Resource.DataError -> {
                            // usually this happening when there is server error
                            Toast.makeText(requireContext(),getString(R.string.unknown_error)
                                ,Toast.LENGTH_SHORT).show()
                            binding.newsProgressBar.visibility = View.GONE
                            binding.searchProgressBar.visibility = View.GONE
                            binding.searchIcon.visibility = View.VISIBLE
                            if (newsList.isEmpty())
                                binding.noDataLayout.visibility = View.VISIBLE

                        }
                        is Resource.Exception -> {
                            // usually this happening when there is no internet
                            Toast.makeText(requireContext(),getString(R.string.no_internet)
                                ,Toast.LENGTH_SHORT).show()
                            binding.newsProgressBar.visibility = View.GONE
                            binding.searchProgressBar.visibility = View.GONE
                            binding.searchIcon.visibility = View.VISIBLE
                            if (newsList.isEmpty())
                                binding.noDataLayout.visibility = View.VISIBLE
                        }
                    }
                }
            })
        )
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            try {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        requireContext(),
                        "com.test.news.utils.ExtendFileProvider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    resultLauncher.launch(takePictureIntent)
                }
            } catch (ex: ActivityNotFoundException) {
                Toast.makeText(
                    requireContext(), getString((R.string.application_not_foun)), Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String =
            SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val storageDir: File? =
            requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }


    override fun onItemClicked(item: Feed, index: Int) {
        showDeleteConfirmationBottomSheetDialog(item, index)
    }

    private fun showDeleteConfirmationBottomSheetDialog(item: Feed, position: Int) {
        // build and show bottom sheet dialog
        val bottomSheetDialog = BottomSheetDialog(requireContext())
        bottomSheetDialog.setContentView(R.layout.confirmatio_bottom_sheet_dialog)
        val yesButton = bottomSheetDialog.findViewById<Button>(R.id.dialog_yes_btn)
        val noButton = bottomSheetDialog.findViewById<Button>(R.id.no_btn)
        bottomSheetDialog.show()
        yesButton?.setOnClickListener {
            bottomSheetDialog.dismiss()
            viewModel.removeItem(item)
            newsList.removeAt(position)
            newsAdapter.submitList(newsList)
            newsAdapter.notifyItemRemoved(position)
            if (newsList.size == 0)
                binding.noDataLayout.visibility = View.VISIBLE
        }
        noButton?.setOnClickListener {

            bottomSheetDialog.dismiss()
        }
    }

    private fun showFillFeedDataBottomSheetDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireContext())
        bottomSheetDialog.setContentView(R.layout.add_feed_bottom_sheet_dialog)
        val titleEditText = bottomSheetDialog.findViewById<EditText>(R.id.title_edit_text)
        val descEditText = bottomSheetDialog.findViewById<EditText>(R.id.description_edit_text)
        val addButton = bottomSheetDialog.findViewById<Button>(R.id.add_btn)
        bottomSheetDialog.show()
        bottomSheetDialog.setCanceledOnTouchOutside(false)
        bottomSheetDialog.setCancelable(false)
        addButton?.setOnClickListener {
            val newFeed =  Feed(
                titleEditText?.text.toString(), descEditText?.text.toString(),
                currentPhotoPath, false, -1L
            )
            newsList.add(
                newFeed
            )
            viewModel.saveFeed(newFeed)
            newsAdapter.submitList(newsList)
            binding.noDataLayout.visibility = View.GONE
            newsAdapter.notifyDataSetChanged()
            // show local notification
            createLocalNotification(titleEditText?.text.toString(), descEditText?.text.toString())
            bottomSheetDialog.dismiss()

        }
    }

    private fun createLocalNotification(title: String, description: String) {
        val builder = NotificationCompat.Builder(requireContext(), getString(R.string.channel_id))
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(title)
            .setContentText(description)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setAutoCancel(true)
        createNotificationChannel()
        with(NotificationManagerCompat.from(requireContext())) {
            // notificationId is a unique int for each notification that you must define
            notify(Random().nextInt(1000), builder.build())
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel =
                NotificationChannel(getString(R.string.channel_id), name, importance).apply {
                    description = descriptionText
                }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                requireActivity().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun scheduleDeleteTask() {
        val timer = Timer()
        val scheduledTask: TimerTask = object : TimerTask() {
            override fun run() {
                if (newsList.size > 10) {
                    // take first 10 items and ignore the rest.
                    newsList = newsList.take(10) as ArrayList<Feed>
                    newsAdapter.submitList(newsList)
                    requireActivity().runOnUiThread {
                        //   updates the UI
                        newsAdapter.notifyDataSetChanged()
                    }
                }
            }
        }
        // schedule the task to run starting now and then every hour...
        timer.schedule(scheduledTask, 0L, 600000) // 1000*10*60 every 10 minutes
    }

    override fun onShareClicked(item: Feed, bitmapFromView: Bitmap?) {
        shareImage(bitmapFromView)
    }

    private fun shareImage(item: Bitmap?) {
        val share = Intent(Intent.ACTION_SEND)
        share.type = "image/*"
        val uri = item?.let { saveImage(it) }
        share.putExtra(Intent.EXTRA_STREAM, uri)
        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(share, "Select"))
    }


    /**
     * Saves the image as PNG to the app's cache directory.
     * @param image Bitmap to save.
     * @return Uri of the saved file or null
     */
    private fun saveImage(image: Bitmap): Uri? {
        val imagesFolder = File(requireActivity().cacheDir, "images")
        var uri: Uri? = null
        try {
            imagesFolder.mkdirs()
            val file = File(imagesFolder, "shared_image.png")
            val stream = FileOutputStream(file)
            image.compress(Bitmap.CompressFormat.PNG, 90, stream)
            stream.flush()
            stream.close()
            uri = FileProvider.getUriForFile(
                requireContext(),
                "com.test.news.utils.ExtendFileProvider",
                file
            )
        } catch (e: IOException) {
            Log.d(
                "FeedFragment",
                "IOException while trying to write file for sharing: " + e.message
            )
        }
        return uri
    }
}
