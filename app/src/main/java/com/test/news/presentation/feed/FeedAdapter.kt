package com.test.news.presentation.feed

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.test.news.R
import com.test.news.databinding.FeedItemBinding
import com.test.news.model.Feed
import com.test.news.utils.listeners.RecyclerItemListener


class FeedAdapter :
    ListAdapter<Feed, FeedAdapter.MyViewHolder>(NewsDiffCallBacks()) {

    lateinit var context: Context
    var dynamicView = false

    private lateinit var listener: RecyclerItemListener<Feed>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding = FeedItemBinding.inflate(layoutInflater, parent, false)
        return MyViewHolder(binding)

    }


    fun setListener(listener: RecyclerItemListener<Feed>) {
        this.listener = listener
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = getItem(position)

        Glide.with(context).load(currentList[position].imageUrl)
            .diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop()
            .into(holder.binding.newsImage).getSize { width, height ->
                val aspectRatio = width.toFloat() / height.toFloat()
                    holder.binding.newsImage.setAspectRatio(aspectRatio)
                if (dynamicView) {
                    val lp = ConstraintLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                    holder.binding.newsImage.layoutParams = lp
                } else {
                    val  imageHeight = context.resources.getDimension(R.dimen.image_height)
                    val lp = ConstraintLayout.LayoutParams(MATCH_PARENT, imageHeight.toInt())
                    holder.binding.newsImage.layoutParams = lp
                }
            }
        holder.bind(item)
    }

    inner class MyViewHolder(val binding: FeedItemBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        fun bind(item: Feed) {
            binding.item = item
            binding.executePendingBindings()
        }

        init {
            itemView.setOnClickListener(this)
            binding.shareIcon.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            if (p0?.id== R.id.share_icon) {
                listener.onShareClicked(getItem(adapterPosition),getBitmapFromView(binding.newsImage))
            }

            else
            listener.onItemClicked(getItem(adapterPosition),adapterPosition)
        }
    }

     fun getBitmapFromView(view: View): Bitmap? {
        val bitmap =
            Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }
    class NewsDiffCallBacks : DiffUtil.ItemCallback<Feed>() {
        override fun areItemsTheSame(
            oldItem: Feed,
            newItem: Feed
        ): Boolean {
            return oldItem.description == newItem.description
        }

        override fun areContentsTheSame(
            oldItem: Feed,
            newItem: Feed
        ): Boolean {
            return newItem.description == oldItem.description
        }
    }


    override fun getItemCount() = currentList.size

}

