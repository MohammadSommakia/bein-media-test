package com.test.news.presentation.host

import androidx.lifecycle.ViewModel
import com.test.news.data.repository.DataRepository
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class HostViewModel  @Inject constructor(
    private val dataRepository: DataRepository,
    override val coroutineContext: CoroutineContext,
) : ViewModel(), CoroutineScope {

}