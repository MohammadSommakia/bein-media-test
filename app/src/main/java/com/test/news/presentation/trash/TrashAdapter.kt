package com.test.news.presentation.trash

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.test.news.databinding.TrashItemBinding
import com.test.news.model.Feed
import com.test.news.utils.listeners.RecyclerItemListener
import java.text.SimpleDateFormat
import java.util.*


class TrashAdapter :
    ListAdapter<Feed, TrashAdapter.MyViewHolder>(TrashDiffCallBacks()) {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding = TrashItemBinding.inflate(layoutInflater, parent, false)
        return MyViewHolder(binding)

    }



    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = getItem(position)
        Glide.with(context).load(currentList[position].imageUrl)
            .diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop()
            .into(holder.binding.newsImage)
        val dateString =
            SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(currentList[position].deletedDate?.let {
                Date(it)
            })
        holder.binding.removedDate.text = "Removed at: $dateString"
        holder.bind(item)
    }

    inner class MyViewHolder(val binding: TrashItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Feed) {
            binding.item = item
            binding.executePendingBindings()
        }
    }

    class TrashDiffCallBacks : DiffUtil.ItemCallback<Feed>() {
        override fun areItemsTheSame(
            oldItem: Feed,
            newItem: Feed
        ): Boolean {
            return oldItem.description == newItem.description
        }

        override fun areContentsTheSame(
            oldItem: Feed,
            newItem: Feed
        ): Boolean {
            return newItem.description == oldItem.description
        }
    }


    override fun getItemCount() = currentList.size

}

