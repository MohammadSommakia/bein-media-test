package com.test.news.presentation.host

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayoutMediator
import com.test.news.R
import com.test.news.databinding.ActivityHostBinding
import com.test.news.utils.ViewModelFactory
import dagger.android.AndroidInjection
import javax.inject.Inject

class HostActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var binding: ActivityHostBinding

    private val viewModel: HostViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(HostViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = ActivityHostBinding.inflate(layoutInflater)
        binding.viewModel = viewModel
        setContentView(binding.root)
        supportActionBar?.hide()
        setupTabView()

    }

    private fun setupTabView() {
        binding.pager.adapter = HostPagerAdapter(this)
        TabLayoutMediator(binding.tabLayout, binding.pager) { tab, position ->
            if (position == 0)
                tab.text = getString(R.string.feed)
            else
                tab.text = getString(R.string.trash)

        }.attach()
    }

}