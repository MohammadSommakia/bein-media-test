package com.test.news.presentation.trash

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.news.data.remote.controller.Resource
import com.test.news.data.remote.responses.NewsResponse
import com.test.news.data.repository.DataRepository
import com.test.news.model.Feed
import com.test.news.utils.Event
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Dispatcher
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class TrashViewModel  @Inject constructor(
    private val dataRepository: DataRepository,
    override val coroutineContext: CoroutineContext,
) : ViewModel(), CoroutineScope {
    val trashList = MutableLiveData<Event<List<Feed>>>()

    fun loadTrashItem() {

        launch {
          val list =   withContext(Dispatchers.IO)
            {
              dataRepository.getDeletedNews()
            }
            trashList.value = Event(list)
        }
    }

    fun deleteItem(feed: Feed)
    {
        launch {
            withContext(Dispatchers.IO)
            {
                dataRepository.deletePermanently(feed)
            }
        }
    }


}