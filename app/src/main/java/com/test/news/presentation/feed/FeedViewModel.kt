package com.test.news.presentation.feed

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.news.data.remote.controller.Resource
import com.test.news.data.remote.responses.NewsResponse
import com.test.news.data.repository.DataRepository
import com.test.news.model.Feed
import com.test.news.utils.Event
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class FeedViewModel @Inject constructor(
    private val dataRepository: DataRepository,
    override val coroutineContext: CoroutineContext,
) : ViewModel(), CoroutineScope {
    val newsResponse = MutableLiveData<Event<Resource<NewsResponse>>>()



    fun geNews(query: String) {
        launch {
            var currentQuery = query
            if (currentQuery.isNotEmpty())
                newsResponse.value = Event(Resource.Loading())
            else
                currentQuery = "tesla"
            val serviceResponse: Resource<NewsResponse> = dataRepository.getNews(currentQuery)
            newsResponse.value = Event(serviceResponse)
        }
    }

    fun removeItem(item: Feed) {
        // marking removed item by deleted = true
        // i am using description as id because the api i am using not provide feed id this is stupid :) .
        launch {
            withContext(Dispatchers.IO)
            {
                item.deleted = true
                item.deletedDate = System.currentTimeMillis()
                item.description.let { dataRepository.updateNewsItem(item.deletedDate!!, it) }
            }

        }
    }

    fun saveFeed(feedList: ArrayList<Feed>) {
        // save feed list into database
        launch {
            withContext(Dispatchers.IO)
            {
                dataRepository.insertFeedList(feedList)
            }
        }
    }

    fun saveFeed(feedList: Feed) {
            // save single item
        launch {
            withContext(Dispatchers.IO)
            {
                dataRepository.insertFeedItem(feedList)
            }
        }
    }

}