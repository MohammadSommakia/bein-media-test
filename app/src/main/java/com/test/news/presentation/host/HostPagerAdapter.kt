package com.test.news.presentation.host

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.test.news.presentation.feed.FeedFragment
import com.test.news.presentation.trash.TrashFragment


class HostPagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return if (position == 0)
            FeedFragment()
        else
            TrashFragment()
    }
}