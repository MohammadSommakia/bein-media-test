package com.test.news.presentation.trash

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.test.news.databinding.FragmentTrashBinding
import com.test.news.model.Feed
import com.test.news.utils.EventObserver
import com.test.news.utils.ViewModelFactory
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class TrashFragment : Fragment() {

    private lateinit var trashList: ArrayList<Feed>
    lateinit var binding: FragmentTrashBinding

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    lateinit var trashAdapter: TrashAdapter

    private val viewModel: TrashViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(TrashViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTrashBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        viewModel.loadTrashItem()
        setupObservers()
        setupSwipeToDeleteFeature()
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        trashList.clear()
        viewModel.loadTrashItem()
    }

    private fun setupObservers() {
        viewModel.trashList.observe(
            requireActivity(),
            EventObserver
                (object : EventObserver.EventUnhandledContent<List<Feed>> {
                override fun onEventUnhandledContent(t: List<Feed>) {
                    setupRecyclerView(t)
                }
            })
        )
    }

    private fun setupRecyclerView(trashList: List<Feed>) {
        this.trashList = trashList as ArrayList<Feed>
        trashAdapter = TrashAdapter()
        trashAdapter.submitList(trashList)
        binding.trashRecyclerView.adapter = trashAdapter
        if (trashList.isEmpty())
            binding.noDataLayout.visibility = View.VISIBLE
        else
            binding.noDataLayout.visibility = View.GONE

    }

    private fun setupSwipeToDeleteFeature() {
        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                v: RecyclerView,
                h: RecyclerView.ViewHolder,
                t: RecyclerView.ViewHolder
            ) = false

            override fun onSwiped(h: RecyclerView.ViewHolder, dir: Int) =
                run {
                    viewModel.deleteItem(trashList[h.adapterPosition])
                    trashList.removeAt(h.adapterPosition)
                    trashAdapter.notifyItemRemoved(h.adapterPosition)
                    if (trashList.isEmpty())
                        binding.noDataLayout.visibility = View.VISIBLE
                }
        }).attachToRecyclerView(binding.trashRecyclerView)
    }
}