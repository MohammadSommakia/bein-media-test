package com.test.news.utils.listeners

interface DialogActionsListener {
    fun onNegativeButtonClicked()
    fun onPositiveButtonClicked()


}