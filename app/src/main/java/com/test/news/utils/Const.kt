package com.test.news.utils

class Const {

    companion object {
        const val DATABASE_NAME: String = "News_Test"
        //Network constants
        const val TIMEOUT_CONNECT = 60L   //In seconds
        const val TIMEOUT_READ = 60L   //In seconds
        const val TIMEOUT_WRITE = 60L   //In seconds
        const val API_KEY = "bb9d807373f3430e8783b69eb993363c"
        const val DATE_FORMAT = "yyyy-MM-dd_HHmmss"

    }
}