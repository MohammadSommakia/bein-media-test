package com.test.news.di.module

import com.test.news.presentation.feed.FeedFragment
import com.test.news.presentation.trash.TrashFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class FragmentModule
{

    @ContributesAndroidInjector
    abstract fun contributeNewsFragment(): FeedFragment

    @ContributesAndroidInjector
    abstract fun contributeTrashFragment(): TrashFragment

}