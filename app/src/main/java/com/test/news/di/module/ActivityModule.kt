package com.test.news.di.module

import com.test.news.presentation.host.HostActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeReadActivity(): HostActivity

}