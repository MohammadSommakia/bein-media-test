package com.test.news.di.module

import androidx.lifecycle.ViewModel
import com.test.news.presentation.host.HostViewModel
import com.test.news.presentation.feed.FeedViewModel
import com.test.news.presentation.trash.TrashViewModel
import com.test.news.utils.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

// Because of @Binds, ViewModelModule needs to be an abstract class

@Module
abstract class ViewModelModule {

// Use @Binds to tell Dagger which implementation it needs to use when providing an interface.
    @Binds
    @IntoMap
    @ViewModelKey(HostViewModel::class)
    abstract fun bindHostViewModel(viewModel: HostViewModel): ViewModel
    
    @Binds
    @IntoMap
    @ViewModelKey(FeedViewModel::class)
    abstract fun bindNewsViewModel(viewModel: FeedViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrashViewModel::class)
    abstract fun bindTrashViewModel(viewModel: TrashViewModel): ViewModel
    
    

}