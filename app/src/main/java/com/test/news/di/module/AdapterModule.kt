package com.test.news.di.module

import com.test.news.presentation.feed.FeedAdapter
import com.test.news.presentation.trash.TrashAdapter
import dagger.Module
import dagger.Provides


@Module
class AdapterModule {

    @Provides
    fun provideNewsAdapter(): FeedAdapter {
        return FeedAdapter()
    }

    @Provides
    fun provideTrashAdapter(): TrashAdapter {
        return TrashAdapter()
    }

}