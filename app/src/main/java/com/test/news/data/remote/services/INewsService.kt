package com.test.news.data.remote.services

import com.test.news.data.remote.responses.NewsResponse
import com.test.news.utils.Urls
import retrofit2.Response
import retrofit2.http.*

interface INewsService {



    @GET(Urls.NEWS)
    suspend fun getNews(@Query("q") query: String, @Query("apikey") key : String): Response<NewsResponse>


}