package com.test.news.data.remote.responses


data class NewsResponse(

    val status: String,
    val totalResults: Long,
    val articles: List<Article>

)

data class Article(
    val author: String,
    val title: String,
    val description: String,
    val url: String,
    val urlToImage: String,
    val publishedAt: String,
    val content: String
)

