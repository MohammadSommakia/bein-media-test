package com.test.news.data.repository

import com.test.news.data.local.repository.LocalRepository
import com.test.news.data.remote.controller.Resource
import com.test.news.data.remote.responses.NewsResponse
import com.test.news.model.Feed
import javax.inject.Inject

class DataRepository
@Inject
constructor(
    private val remoteRepository: RemoteRepository,
    private val localRepository: LocalRepository
) : DataSource {

    override suspend fun getNews(query: String): Resource<NewsResponse> {
        return remoteRepository.getNews(query)
    }

    override fun updateNewsItem(deletedDate: Long,description: String) {
        localRepository.updateFeedItem(deletedDate,description)
    }

    override fun getDeletedNews() : List<Feed>{
        return localRepository.getDeletedFeed()
    }

    override fun deletePermanently(item: Feed) {
        localRepository.deletePermanently(item)
    }

    override fun insertFeedList(feeds: ArrayList<Feed>) {
        localRepository.insertFeedsList(feeds)
    }

    override fun insertFeedItem(feed: Feed) {
        localRepository.insertFeedItem(feed)

    }

}

