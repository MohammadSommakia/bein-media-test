package com.test.news.data.remote.requests


@Suppress("unused")
class SignUpRequest {

    var email:String? = null

    var uid:String? = null

    var password:String? = null

    var googleAuth:Boolean? = null
}