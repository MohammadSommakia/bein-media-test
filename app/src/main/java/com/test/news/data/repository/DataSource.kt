package com.test.news.data.repository

import com.test.news.data.remote.controller.Resource
import com.test.news.data.remote.responses.NewsResponse
import com.test.news.model.Feed

interface DataSource {

    suspend fun getNews(query : String): Resource<NewsResponse>
    fun updateNewsItem(deletedDate: Long,description: String)
    fun getDeletedNews(): List<Feed>
    fun deletePermanently(item : Feed)
    fun insertFeedList(feed : ArrayList<Feed>)
    fun insertFeedItem(feed : Feed)
}