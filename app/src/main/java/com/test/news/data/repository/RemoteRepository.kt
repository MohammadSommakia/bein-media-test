package com.test.news.data.repository

import com.google.gson.Gson
import com.test.news.data.remote.controller.ErrorManager
import com.test.news.data.remote.controller.IRestApiManager
import com.test.news.data.remote.controller.Resource
import com.test.news.data.remote.controller.ServiceGenerator
import com.test.news.data.remote.services.INewsService
import com.test.news.data.remote.responses.NewsResponse
import com.test.news.utils.Const
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import javax.inject.Inject


class RemoteRepository
@Inject constructor(private val serviceGenerator: ServiceGenerator, private val gson: Gson) :
    IRestApiManager {

    @Suppress("BlockingMethodInNonBlockingContext")

    override suspend fun getNews(query : String): Resource<NewsResponse> {
        val authService = serviceGenerator.createService(INewsService::class.java)
        try {
            val response = authService.getNews(query,Const.API_KEY)

            return if (response.isSuccessful) {
                //Do something with response e.g show to the UI.
                Resource.Success(response.body() as NewsResponse)
            } else {
                val errorBody = gson.fromJson(
                    response.errorBody()?.string(),
                    ErrorManager::class.java
                )
                errorBody.code = response.code()
                Resource.DataError(errorBody)
            }
        } catch (e: HttpException) {
            return Resource.Exception(e.message() as String)
        } catch (e: Throwable) {
            return Resource.Exception(errorMessage = e.message as String)
        }
        catch (e: SocketTimeoutException) {
            return Resource.Exception(errorMessage = e.message as String)
        }
        catch (e: IOException)
        {
            return Resource.Exception(errorMessage = e.message as String)

        }
    }
}