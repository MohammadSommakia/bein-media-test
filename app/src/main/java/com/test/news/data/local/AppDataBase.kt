package com.test.news.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.test.news.data.local.repository.dao.FeedDao
import com.test.news.model.Feed

@Database(
    entities = [Feed::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun newsDao(): FeedDao
}