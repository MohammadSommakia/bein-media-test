package com.test.news.data.remote.controller

import com.test.news.data.remote.responses.NewsResponse

internal interface IRestApiManager {

    suspend fun getNews(query : String): Resource<NewsResponse>
}